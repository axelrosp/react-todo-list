import React, { useContext } from 'react'
import AccountCircle from "@material-ui/icons/AccountCircle";
import Avatar from '@material-ui/core/Avatar';
import Menu, { MenuProps } from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { createStyles, makeStyles, Theme, withStyles } from '@material-ui/core/styles';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { ExitToAppOutlined, PersonOutline } from '@material-ui/icons';
import UserModel from '../../models/UserModel';
import { FirebaseContext } from '../../firebase';

const StyledMenu = withStyles({
  paper: {
    border: '1px solid #d3d4d5',
  },
})((props: MenuProps) => (
  <Menu
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'center',
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'center',
    }}
    {...props}
  />
));

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    smallIcon: {
      width: theme.spacing(4),
      height: theme.spacing(4),
      "&:hover": {
        boxShadow: "0.5px 0.5px 5px black",
        cursor: "pointer"
      }
    },
  }),
);

export interface RightButtonsProps {
  user: UserModel | null,
}

const RightButtons: React.FC<RightButtonsProps> = (props) => {
  const { user } = props
  const classes = useStyles()
  const { firebaseController } = useContext(FirebaseContext)
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

  const handleLogout = () => {
    firebaseController.logout()
  }

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null)
  }

  return (
    <>
      {user ?
        <Avatar className={classes.smallIcon} variant={"circle"} src={user.photoURL} onClick={handleClick} /> :
        <AccountCircle className={classes.smallIcon} />
      }
      <StyledMenu
        id="customized-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
        autoFocus={false}
      >
        <MenuItem onClick={handleLogout}>
          <ListItemIcon>
            <ExitToAppOutlined fontSize="small" />
          </ListItemIcon>
          <ListItemText primary="Logout" />
        </MenuItem>
      </StyledMenu>
    </>
  );
}

export default RightButtons;