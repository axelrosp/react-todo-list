import React from 'react'
import { Button, CircularProgress, isWidthDown, makeStyles } from '@material-ui/core';
import { UserReducerType } from '../../store/reducers/userReducers';
import theme from '../../providers/theme';
import Avatar from '@material-ui/core/Avatar';
import { CustomColors } from '../../utils/ColorUtils';

const useStyles = makeStyles({
    btn: {
        border: `1px solid #c3c0c0`,
        padding: 2,
        paddingRight: 8,
        paddingLeft: 8,
        width: "50%"
    },
    btnTitle: {
        marginLeft: 8,
        fontWeight: "bold",
    },
    avatar: {
        width: theme.spacing(3),
        height: theme.spacing(3),
        paddingRigt: 24
    },
});

export interface SocialMediaButtonProps {
    userState?: UserReducerType
    onClick: (e: React.MouseEvent) => void
}

const SocialMediaButton: React.FC<SocialMediaButtonProps> = (props) => {
    const { userState, onClick } = props
    const classes = useStyles()

    return (
        <Button
            className={classes.btn}
            onClick={onClick}
        >
            <>
                {userState && userState.loading ?
                    <CircularProgress size={25} /> :
                    <>
                        <Avatar className={classes.avatar} src={require("../../assets/img/google.png")} />
                        <text className={classes.btnTitle}>Google</text>
                    </>
                }
            </>
        </Button>
    );
}

export default SocialMediaButton;