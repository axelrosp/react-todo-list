/* eslint-disable no-use-before-define */
import React from 'react';
import Autocomplete from '@material-ui/lab/Autocomplete';
import Avatar from '@material-ui/core/Avatar';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import UserModel from '../../../models/UserModel';
import { CSSProperties } from '@material-ui/core/styles/withStyles';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        container: {
            padding: "0 !important"
        },
        smallIcon: {
            width: theme.spacing(3),
            height: theme.spacing(3),
            marginRight: 4
        },
        typography: {
            fontSize: "0.8rem"
        }
    }),
);

interface AutoCompleteInviteesProps {
    users: UserModel[],
    invitees: UserModel[],
    style?: CSSProperties,
    onSetInvitees: (invitees: UserModel[]) => void
}

const AutoCompleteInvitees: React.FC<AutoCompleteInviteesProps> = (props) => {
    const { users, onSetInvitees, invitees, style } = props
    const classes = useStyles()

    const handleChange = (options: UserModel[]) => {
        onSetInvitees(options)
    }

    return (
        <Autocomplete
            id="autocomplete-invitees-outlined"
            size="small"
            className={classes.container}
            autoComplete
            multiple
            options={users}
            getOptionLabel={option => (option.email)}
            value={invitees}
            onChange={(event: any, options: UserModel[]) => handleChange(options)}
            filterSelectedOptions
            renderOption={option =>
                <React.Fragment>
                    <Avatar className={classes.smallIcon} variant={"circle"} src={option.photoURL} />
                    <Typography className={classes.typography}>{option.email}</Typography>
                </React.Fragment>
            }
            renderInput={(params) => (
                <TextField
                    {...params}
                    variant="outlined"
                    label="Invitees"
                />
            )}
            style={style}
        />
    );
}

export default AutoCompleteInvitees