import React from "react"
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import { CSSProperties } from "@material-ui/core/styles/withStyles";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: "flex",
            height: theme.mixins.toolbar.height,
            padding: theme.mixins.toolbar.padding
        },
        container: {
            boxShadow: "none"
        },
        toolBar: {
            minHeight: 25,
            boxShadow: "none !important"
        },
    }),
);

export interface NavbarGenericProps {
    style?: CSSProperties
}

const NavbarGeneric: React.FC<NavbarGenericProps> = (props) => {
    const { style, children } = props
    const classes = useStyles();

    return (
        <div className={classes.root} style={style}>
            <AppBar position="static" className={classes.container} style={{ background: style?.background }}>
                <Toolbar className={classes.toolBar} >
                    {children}
                </Toolbar>
            </AppBar>
        </div>
    );
}

export default NavbarGeneric;