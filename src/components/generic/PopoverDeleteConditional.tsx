import { makeStyles, PopoverOrigin, Typography } from '@material-ui/core';
import React from 'react'
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import PopoverGeneric from './PopoverGeneric';
import { shadeColor } from '../../utils/ColorUtils';
import { CustomColors } from '../../utils/ColorUtils';

const useStyles = makeStyles(theme => ({
    container: {
        padding: 8,
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        maxWidth: 200
    },
    contentText: {
        textAlign: "center"
    },
    contentButtons: {
        marginTop: 4,
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",

    },
    button: {
        color: "#FFF",
        fontWeight: "bold",
        width: "100%",
        padding: 2
    },
    delete: {
        background: CustomColors.deleteColor,
        marginLeft: 2,
        '&:hover': {
            background: shadeColor(CustomColors.deleteColor, -25)
        }
    },
    cancel: {
        background: theme.palette.primary.main,
        color: theme.palette.primary.contrastText,
        marginRight: 2,
        '&:hover': {
            background: shadeColor(theme.palette.primary.main, -25)
        }
    }
}))

export interface PopoverDeleteConditionalProps {
    id: string,
    title: string,
    open: boolean,
    anchorEl: HTMLElement,
    transformOrigin?: PopoverOrigin,
    onAccept: () => void,
    onCancel: () => void
}

const PopoverDeleteConditional: React.FC<PopoverDeleteConditionalProps> = (props) => {
    const { id, title, open, anchorEl, transformOrigin, onAccept, onCancel } = props
    const classes = useStyles()

    return (
        <PopoverGeneric
            id={id}
            open={open}
            anchorEl={anchorEl}
            transformOrigin={transformOrigin}
            onCancel={onCancel}
            clickAwayDetection
        >
            <div className={classes.container}>
                <div className={classes.contentText}>
                    <Typography>{title}</Typography>
                </div>
                <div className={classes.contentButtons}>
                    <Button
                        variant="contained"
                        className={`${classes.button} ${classes.cancel}`}
                        onClick={onCancel}
                    >
                        Cancel
                    </Button>
                    <Button
                        variant="contained"
                        className={`${classes.button} ${classes.delete}`}
                        startIcon={<DeleteIcon />}
                        onClick={onAccept}
                    >
                        Delete
                    </Button>
                </div>
            </div>
        </PopoverGeneric >
    );
}

export default PopoverDeleteConditional;

