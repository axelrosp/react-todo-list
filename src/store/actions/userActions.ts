import firebaseController from "../../firebase/FirebaseController";
import UserModel from "../../models/UserModel"
import { LOADING_USER, LOGIN_USER, LOGOUT_USER } from "../actionTypes/userTypes"
import { UserReducerType } from "../reducers/userReducers"

export function initUserAction() {
    return async (dispatch: any, getState: () => UserReducerType) => {
        firebaseController.auth.onAuthStateChanged((user: any) => {
            if (user) {
                dispatch(fetchLoginUser(user))
            }
            else {
                dispatch(fetchLogoutUser())
            }
        });
    }
}

export function loadingUserAction() {
    return async (dispatch: any, getState: () => UserReducerType) => {
        dispatch(loadingUser())
    }
}

const loadingUser = () => ({
    type: LOADING_USER,
    payload: true
})

const fetchLoginUser = (user: UserModel) => ({
    type: LOGIN_USER,
    payload: user
})

const fetchLogoutUser = () => ({
    type: LOGOUT_USER,
    payload: null
})