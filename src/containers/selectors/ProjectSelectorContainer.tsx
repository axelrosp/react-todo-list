import React, { useEffect, useMemo, useState } from 'react'
import { useSelector } from 'react-redux';
import SelectorGeneric from '../../components/generic/SelectorGeneric';
import useProjects from '../../firebase/firestore/useProjects';
import useSearchParams from '../../hooks/useSearchParams';
import { ProjectModel } from '../../models/ProjectModel';
import { ReducerType } from '../../store/reducers';
import { getUser } from '../../store/reducers/userReducers';

export interface ProjectSelectorContainerProps {

}

const ProjectSelectorContainer: React.FC<ProjectSelectorContainerProps> = () => {
    const label = "PROJECT"
    const { searchParams, history } = useSearchParams()
    const [selectedValue, setSelectedValue] = useState<string | null>(null)
    const user = useSelector((state: ReducerType) => getUser(state.userReducer))
    const { projectsData } = useProjects(user?.uid)

    const projects: ProjectModel[] = projectsData.data
    const selectedProjectId = searchParams.getProjectId()

    useEffect(() => {
        setSelectedValue(selectedProjectId)
    }, [selectedProjectId, JSON.stringify(projects)])

    const handleChangeValue = (projectId: string) => {
        if (projectId !== "-1") {
            searchParams.setProjectId(projectId)
            setSelectedValue(projectId)
        }
        else {
            searchParams.clearProject()
            setSelectedValue(null)
        }
        history.push(searchParams.toString())
    }
    return (
        <SelectorGeneric
            label={label}
            values={projects}
            onChangeValue={handleChangeValue}
            selectedValue={selectedValue}
        />
    );
}

export default ProjectSelectorContainer;