import React, { useState, useMemo } from 'react'
import { useSelector } from 'react-redux';
import AddCategoryPopover from '../../components/board/Category/AddCategoryPopover';
import AddProjectPopover from '../../components/board/Project/AddProjectPopover';
import BoardNavbar from '../../components/board/BoardNavbar';
import useCategories from '../../firebase/firestore/useCategories';
import useProjects from '../../firebase/firestore/useProjects';
import CategoryModel from '../../models/CategoryModel';
import { ProjectModel } from '../../models/ProjectModel';
import { ReducerType } from '../../store/reducers';
import { getUser } from '../../store/reducers/userReducers';

export interface BoardNavbarContainerProps {
    project: ProjectModel | null,
    onEditProject: (value: boolean) => void,
    shifted: boolean
}

const BoardNavbarContainer: React.FC<BoardNavbarContainerProps> = (props) => {
    const { project, shifted, onEditProject } = props
    const [openAddProject, setOpenAddProject] = useState<HTMLElement | null>(null)
    const [openAddCategory, setOpenAddCategory] = useState<HTMLElement | null>(null)

    const user = useSelector((state: ReducerType) => getUser(state.userReducer))

    const { addProject } = useProjects(user?.uid)
    const { categoriesData, addCategory, updateCategories } = useCategories()


    //Projects
    const handleOpenAddProject = (element: HTMLElement) => {
        setOpenAddProject(element)
    }

    const handleCancelAddProject = () => {
        setOpenAddProject(null)
    }

    const handleOpenEditProject = (open: boolean) => {
        onEditProject(open)
    }

    const handleAddProject = (project: ProjectModel) => {
        setOpenAddProject(null)
        addProject(project)
    }

    //Categories
    const handleOpenAddCategory = (element: HTMLElement) => {
        setOpenAddCategory(element)
    }

    const handleCancelAddCategory = () => {
        setOpenAddCategory(null)
    }

    const handleAddCategory = (category: CategoryModel) => {
        const categories = [...categoriesData.data]
        const categoriesAux = categories.map(category => {
            category.position += 1
            return category
        })
        updateCategories(categoriesAux)
        addCategory(category)
        setOpenAddCategory(null)
    }

    return (
        <>
            {openAddProject && user &&
                <AddProjectPopover
                    id="add-project"
                    open={true}
                    user={user}
                    anchorEl={openAddProject}
                    onCancel={handleCancelAddProject}
                    onAddProject={handleAddProject}
                />
            }
            {openAddCategory && project &&
                <AddCategoryPopover
                    id="add-category"
                    project={project}
                    anchorEl={openAddCategory}
                    onCancel={handleCancelAddCategory}
                    onAddCategory={handleAddCategory}
                    transformOrigin={{
                        vertical: 'top',
                        horizontal: 'right',
                    }}
                />
            }
            <BoardNavbar
                onAddProject={handleOpenAddProject}
                onAddCategory={handleOpenAddCategory}
                onEditProject={handleOpenEditProject}
                project={project}
                shifted={shifted}
            />
        </>
    );
}

export default BoardNavbarContainer;