import React, { useEffect, useMemo, useState } from 'react'
import { useSelector } from 'react-redux';
import EditProejctDrawer from "../../components/board/Project/EditProjectDrawer"
import { Anchor } from '../../components/generic/DrawerGeneric';
import PopoverDeleteConditional from '../../components/generic/PopoverDeleteConditional';
import { SelectedPopoverProps } from '../../components/generic/PopoverGeneric';
import useProjects from '../../firebase/firestore/useProjects';
import useUsers from '../../firebase/firestore/useUsers';
import useSearchParams from '../../hooks/useSearchParams';
import { ProjectModel } from '../../models/ProjectModel';
import UserModel from '../../models/UserModel';
import { ReducerType } from '../../store/reducers';
import { getUser } from '../../store/reducers/userReducers';

export interface EditProjectDrawerContainerProps {
    project: ProjectModel,
    anchor: Anchor,
    open: boolean,
    onChangeOpenState: (open: boolean) => void
}

const EditProjectDrawerContainer: React.FC<EditProjectDrawerContainerProps> = (props) => {
    const { project, anchor, open, onChangeOpenState } = props
    const { searchParams, history } = useSearchParams()

    const user = useSelector((state: ReducerType) => getUser(state.userReducer))
    const { updateProject, deleteProject } = useProjects(user?.uid)
    const { usersData, getUserById } = useUsers()

    const [userOwner, setUserOwner] = useState<UserModel | null>(null)
    const [availableUsers, setAvailableUsers] = useState<UserModel[]>([])
    const [deleteProjectSelected, setDeleteProjectSelected] = useState<null | SelectedPopoverProps>(null)

    useEffect(() => {
        getUserById(project.createdBy).then(user => setUserOwner(user))
    }, [project])

    useMemo(() => {
        const users = usersData.data.filter(
            userData => (
                userData.uid !== project.createdBy
            )
        )
        setAvailableUsers(users)
    }, [JSON.stringify(usersData.data), project])

    const handleSaveEdit = (project: ProjectModel) => {
        updateProject(project)
        onChangeOpenState(false)
    }

    const handleDeleteProject = (id: string, anchorEl: HTMLElement) => {
        setDeleteProjectSelected({ id, anchorEl })
    }

    const handleCancelDeleteProject = () => {
        setDeleteProjectSelected(null)
    }

    const handleAcceptDeleteProject = () => {
        if (deleteProjectSelected && project) {
            deleteProject(project.id)
        }
        setDeleteProjectSelected(null)
        searchParams.clearProject()
        history.push(searchParams.toString())
    }

    return (
        <>
            {deleteProjectSelected &&
                <PopoverDeleteConditional
                    id={"delete-project"}
                    open={deleteProjectSelected !== null}
                    title={"Delete this project and all related data?"}
                    anchorEl={deleteProjectSelected.anchorEl}
                    onAccept={handleAcceptDeleteProject}
                    onCancel={handleCancelDeleteProject}
                    transformOrigin={{
                        vertical: 'top',
                        horizontal: 'right',
                    }}
                />
            }
            {user && userOwner &&
                <EditProejctDrawer
                    anchor={anchor}
                    open={open}
                    user={user}
                    project={project}
                    users={availableUsers}
                    owner={userOwner}
                    onSaveEdit={handleSaveEdit}
                    onChangeOpenState={onChangeOpenState}
                    onDeleteProject={handleDeleteProject}
                />
            }
        </>
    );
}

export default EditProjectDrawerContainer;