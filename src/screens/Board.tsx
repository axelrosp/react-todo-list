import React, { useEffect, useState } from 'react'
import { makeStyles } from '@material-ui/core';
import CategoriesContainer from '../containers/board/CategoriesContainer';
import useSearchParams from '../hooks/useSearchParams';
import EditProjectDrawerContainer from '../containers/board/EditProjectDrawerContainer';
import { ProjectModel } from '../models/ProjectModel';
import BoardNavbarContainer from '../containers/board/BoardNavbarContainer';
import { useSelector } from 'react-redux';
import { getUser } from '../store/reducers/userReducers';
import { ReducerType } from '../store/reducers';
import useProjects from '../firebase/firestore/useProjects';

const useStyles = makeStyles({
    container: {
        display: "flex",
        flexDirection: "column"
    },
    content: {
        marginTop: 8
    }
})

const Board: React.FC = () => {
    const classes = useStyles()
    const user = useSelector((state: ReducerType) => getUser(state.userReducer))

    const [isEditProject, setIsEditProject] = useState<boolean>(false)
    const [project, setProject] = useState<ProjectModel | null>(null)

    const { searchParams } = useSearchParams()
    const { getProjectById } = useProjects(user?.uid)
    const projectId = searchParams.getProjectId()

    useEffect(() => {
        setIsEditProject(false)
        if (projectId && user)
            getProjectById(user, projectId)
                .then(project => setProject(project))
                .catch(() => setProject(null))
        else
            setProject(null)
    }, [projectId])

    const handleChangeEditProject = (open: boolean) => {
        setIsEditProject(open)
    }

    return (
        <div className={classes.container}>
            {project &&
                <EditProjectDrawerContainer
                    open={isEditProject}
                    anchor={"right"}
                    onChangeOpenState={handleChangeEditProject}
                    project={project}
                />
            }
            <BoardNavbarContainer project={project} onEditProject={handleChangeEditProject} shifted={isEditProject} />

            <div className={classes.content} >
                {project &&
                    <CategoriesContainer project={project} />
                }
            </div>
        </div>
    );
}

export default Board;