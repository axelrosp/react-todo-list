import HttpError from "./HttpError";

interface GenericData<T> {
    error: HttpError | null;
    isFetching: boolean;
    data: T[];
}

export default GenericData