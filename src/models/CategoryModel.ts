import DraggableModel from "./DraggableModel";
import GenericModel from "./GenericModel"

export const CATEGORIES_COLLECTION = "categories"
export const CategoryModelColumns = {
    project: "project",
    color: "color"
}

export default interface CategoryModel extends GenericModel, DraggableModel {
    project: string,
    color: string,
}