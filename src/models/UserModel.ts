export const USERS_COLLECTION = "users"

export default interface UserModel {
    uid: string,
    displayName: string,
    photoURL: string,
    email: string
}