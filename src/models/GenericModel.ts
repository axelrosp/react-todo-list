export default interface GenericModel {
    id: string,
    name: string
}