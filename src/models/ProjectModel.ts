import GenericModel from "./GenericModel"
import UserModel from "./UserModel";

export const PROJECTS_COLLECTION = "projects"
export const ProjectModelColumns = {
    createdBy: "createdBy",
    invitees: "invitees"
}

export interface ProjectModel extends GenericModel {
    createdBy: string,
    invitees: string[]
}

export interface ProjectModelPopulated extends GenericModel {
    createdBy: string,
    invitees: UserModel[]
}