import DraggableModel from "./DraggableModel";
import GenericModel from "./GenericModel";

export const TASKS_COLLECTION = "tasks"
export const TaskModelColumns = {
    description: "description",
    category: "category",
    assignee: "assignee"
}

export default interface TaskModel extends GenericModel, DraggableModel {
    description?: string,
    category: string,
    assignee?: string | null
}