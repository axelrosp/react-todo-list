import React from 'react';
import { useSelector } from 'react-redux';
import LoginContainer from './containers/login/LoginContainer';
import { ReducerType } from './store/reducers';
import { getUser } from './store/reducers/userReducers'
import useAuth from "./firebase/hooks/useAuth"
import { BrowserRouter as Router } from 'react-router-dom';
import Routes from './routes/routes';
import NavBar from './components/navBar/NavBar';

function App() {
  useAuth()
  const user = useSelector((state: ReducerType) => getUser(state.userReducer))
  return (
    <Router>
      <>
        {!user ? <LoginContainer /> :
          <div>
            <NavBar />
            <Routes />
          </div>
        }
      </>
    </Router>
  )
};


export default App;
