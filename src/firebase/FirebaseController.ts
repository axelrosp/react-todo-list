import app from "firebase";
import "firebase/auth";
import "firebase/firestore";
import "firebase/storage";
import UserModel from "../models/UserModel";
import firebaseConfig from "./config";

export type IFirebaseController = {
	login: (callback?: (user: UserModel) => void) => void
	logout: () => Promise<Boolean>
}

class FirebaseController implements IFirebaseController {
	auth: any
	db: app.firestore.Firestore
	storage: any
	googleProvider: any
	constructor() {
		if (!app.apps.length) {
			app.initializeApp(firebaseConfig);
		}
		this.auth = app.auth();
		this.db = app.firestore();
		this.storage = app.storage();
		this.googleProvider = new app.auth.GoogleAuthProvider();
	}

	login(callback?: any) {
		this.auth.signInWithPopup(this.googleProvider)
			.then((result: any) => {
				const userResult = result.user
				if (callback) {
					const user: UserModel = {
						id: userResult.uid,
						...userResult
					}
					callback(user)
				}
			})
	}

	async logout() {
		return await this.auth.signOut();
	}
}

const firebaseController = new FirebaseController();
export default firebaseController;
