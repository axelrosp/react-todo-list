import React, { useEffect, useState } from 'react';
import FirebaseController from '..';
import CategoryModel, { CATEGORIES_COLLECTION } from '../../models/CategoryModel';
import TaskModel, { TaskModelColumns, TASKS_COLLECTION } from '../../models/TaskModel';
import { USERS_COLLECTION } from '../../models/UserModel';
import GenericData from '../../utils/typeUtils/GenericData';

function useTasks() {
    const [categoryIds, setCategoryIds] = useState<string[]>([])
    const [tasksData, setTasksData] = React.useState<GenericData<TaskModel>>({
        error: null,
        isFetching: true,
        data: [],
    });

    const collection = FirebaseController
        .db
        .collection(TASKS_COLLECTION)

    useEffect(() => {
        if (categoryIds.length > 0) {
            const categoriesRef = categoryIds.map(category => FirebaseController.db.doc(`${CATEGORIES_COLLECTION}/` + category))
            const unsubscribe = collection.where(TaskModelColumns.category, "in", categoriesRef).onSnapshot(snapshotManager);
            return unsubscribe;
        }
    }, [categoryIds]);

    function initDataTasks(categoryIds: string[]) {
        setCategoryIds(categoryIds)
    }

    function snapshotManager(snapshot: any, error?: Error | null) {
        if (error) {
            setTasksData(
                {
                    error: { name: error.name, message: error.message, status: 400 },
                    isFetching: false,
                    data: [],
                }
            )
        }
        else {
            setTasksData(
                {
                    error: null,
                    isFetching: false,
                    data: snapshot.docs
                        .map((doc: any) => {
                            return {
                                id: doc.id,
                                name: doc.data().name,
                                position: doc.data().position,
                                description: doc.data().description,
                                category: doc.data().category?.id,
                                assignee: doc.data().assignee?.id,
                            }
                        }),
                }
            )
        }
    }

    async function getTasks(category: string) {
        return await collection.where(category, "==", category).get()
    }

    function addTask(task: TaskModel, callback?: any) {
        const batch: firebase.firestore.WriteBatch = FirebaseController.db.batch()
        const ref = collection.doc();
        batch.set(ref, {
            name: task.name,
            position: task.position,
            description: task.description ? task.description : "",
            category: FirebaseController.db.doc(`${CATEGORIES_COLLECTION}/` + task.category),
            assignee: FirebaseController.db.doc(`${USERS_COLLECTION}/` + task.assignee),
        })
        batch.commit().then(() => callback && callback())
    }

    function updateTasks(tasks: TaskModel[], callback?: any) {
        const batch: firebase.firestore.WriteBatch = FirebaseController.db.batch()
        tasks.forEach(task => {
            const ref = collection.doc(task.id)
            batch.update(ref, {
                name: task.name,
                position: task.position,
                description: task.description ? task.description : "",
                category: FirebaseController.db.doc(`${CATEGORIES_COLLECTION}/` + task.category),
                assignee: task.assignee ? FirebaseController.db.doc(`${USERS_COLLECTION}/` + task.assignee) : null,
            })
        })
        batch.commit().then(() => callback && callback())
    }

    function updateTask(task: TaskModel, callback?: any) {
        collection.doc(task.id).update({
            name: task.name,
            position: task.position,
            description: task.description ? task.description : "",
            category: FirebaseController.db.doc(`${CATEGORIES_COLLECTION}/` + task.category),
            assignee: task.assignee ? FirebaseController.db.doc(`${USERS_COLLECTION}/` + task.assignee) : null,
        })
    }

    function deleteTask(id: string, callback?: any) {
        collection.doc(id).delete().then(() => callback && callback())
    }

    async function deleteTasks(categories: CategoryModel[], callback?: any) {

        const categoriesMapped = categories.map(cat => FirebaseController.db.doc(`${CATEGORIES_COLLECTION}/` + cat.id))

        if (categoriesMapped.length > 0) {
            const batch: firebase.firestore.WriteBatch = FirebaseController.db.batch()
            await Promise.all([collection.where(TaskModelColumns.category, "in", categoriesMapped).get()
                .then(
                    result => {
                        result.docs.map(async task => {
                            const ref = collection.doc(task.id)
                            await batch.delete(ref)
                        })
                    }
                )
            ]).then(() => {
                batch.commit().then(() => { callback && callback() })
            })
        }
        else {
            callback && callback()
        }
    }

    return {
        tasksData,
        initDataTasks,
        getTasks,
        addTask,
        deleteTask,
        deleteTasks,
        updateTasks,
        updateTask
    }
}

export default useTasks;