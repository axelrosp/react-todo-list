import React, { useEffect } from 'react';
import FirebaseController from '..';
import { ProjectModel, ProjectModelColumns, ProjectModelPopulated, PROJECTS_COLLECTION } from '../../models/ProjectModel';
import UserModel, { USERS_COLLECTION } from '../../models/UserModel';
import GenericData from '../../utils/typeUtils/GenericData';
import useCategories from './useCategories';
import useUsers from './useUsers';

function useProjects(userId?: string) {
    const [projectsData, setProjectssData] = React.useState<GenericData<ProjectModel>>({
        error: null,
        isFetching: true,
        data: [],
    });
    const { getUserById } = useUsers()
    const { categoriesData, deleteCategories } = useCategories()

    const collection = FirebaseController
        .db
        .collection(PROJECTS_COLLECTION)

    useEffect(() => {
        const userRef = FirebaseController.db.doc(`${USERS_COLLECTION}/` + userId)
        const unsubscribe = collection.where(ProjectModelColumns.invitees, "array-contains", userRef).onSnapshot(snapshotManager);
        return unsubscribe;
    }, []);

    function snapshotManager(snapshot: any, error?: Error | null) {
        if (error) {
            setProjectssData(
                {
                    error: { name: error.name, message: error.message, status: 400 },
                    isFetching: false,
                    data: [],
                }
            )
        }
        else {
            setProjectssData(
                {
                    error: null,
                    isFetching: false,
                    data: snapshot.docs
                        .map((doc: any) => {
                            return {
                                id: doc.id,
                                name: doc.data().name,
                                createdBy: doc.data().createdBy.id,
                                invitees: doc.data().invitees.map((invitee: any) => invitee.id),
                            }
                        }),
                }
            )
        }
    }

    async function loadInvitees(users: any) {
        return await Promise.all(users.map((user: any) => (
            getUserById(user.id)
        )))
    }

    async function getProjectById(user: UserModel, id: string) {
        var docRef = collection.doc(id);

        return await docRef.get().then(function (doc) {
            if (doc.exists) {
                let project: ProjectModel | null = null
                const data = doc.data()
                if (data) {
                    project = {
                        id: doc.id,
                        name: data.name,
                        createdBy: data.createdBy && data.createdBy.id,
                        invitees: data.invitees?.map((invitee: any) => invitee.id)
                    }
                }
                if (project?.createdBy === user.uid || project?.invitees.includes(user.uid))
                    return project
                throw new Error("Permissions denied")
            } else {
                throw new Error("No such document");
            }
        }).catch(function (error) {
            throw new Error("Error getting document");
        });
    }

    async function addProject(project: ProjectModel) {
        const batch: firebase.firestore.WriteBatch = FirebaseController.db.batch()
        const ref = collection.doc();
        const createdByRef = FirebaseController.db.doc(`${USERS_COLLECTION}/${project.createdBy}`)
        const invitees = project.invitees.map(invitee => (
            FirebaseController.db.doc(`${USERS_COLLECTION}/${invitee}`)
        ))
        invitees.push(createdByRef)

        batch.set(ref, {
            name: project.name,
            createdBy: FirebaseController.db.doc(`${USERS_COLLECTION}/${project.createdBy}`),
            invitees: invitees
        })
        batch.commit()
    }

    function updateProject(project: ProjectModel) {
        if (!project.invitees.includes(project.createdBy)) project.invitees.push(project.createdBy)
        collection.doc(project.id).update({
            name: project.name,
            createdBy: FirebaseController.db.doc(`${USERS_COLLECTION}/${project.createdBy}`),
            invitees: project.invitees.map(invitee => (
                FirebaseController.db.doc(`${USERS_COLLECTION}/${invitee}`)
            ))
        })
    }

    function deleteProject(id: string, callback?: any) {
        const categories = [...categoriesData.data]
        deleteCategories(categories, () => {
            collection.doc(id).delete().then(() => callback && callback())
        })
    }

    return {
        projectsData,
        getProjectById,
        addProject,
        updateProject,
        deleteProject
    }
}

export default useProjects;